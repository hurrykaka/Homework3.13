import java.util.Scanner;

public class H3 {

	public static void main(String[] args) {
		// 用户输入任意一个整数，求其所有位上的数字和并输出
		// 比如123，和为6,10000和为1,用for完成
		Scanner A = new Scanner(System.in);
		System.out.print("输入任意一个整数");
		int a = A.nextInt(), sum = 0;
		for (; a != 0; a = a / 10) {
			sum = sum + a % 10;
		}
		System.out.print(sum);
	}
}
