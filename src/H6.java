import java.util.Scanner;

public class H6 {
	public static void main(String[] args) {
		// 输入一个整数
		// 判断是否是质数（定义是只能被1和自身整除的数，比如3，7，2等）
		Scanner A = new Scanner(System.in);
		System.out.print("输入任意一个整数");
		int a = A.nextInt();
		for (int i = 2;; i++) {
			if (a % i == 0 && a != i)
				System.out.print(a + " 是质数");
			else
				System.out.print(a + "不是质数");
			break;
		}
	}
}
