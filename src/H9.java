import java.util.Scanner;

public class H9 {

	public static void main(String[] args) {
		// 输入一个整数，判断其是否是回文数
		// 回文数是指这个数颠倒后，和原数相同那个。
		Scanner A = new Scanner(System.in);
		System.out.print("输入任意一个整数");
		int a = A.nextInt(), sum = 0;
		for (; a != 0; a /= 10) {
			sum = sum * 10 + a % 10;
		}
		if (sum == a) {
			System.out.print("是");
		} else
			System.out.print("不是");

	}

}
