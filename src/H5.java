import java.util.Scanner;

public class H5 {

	public static void main(String[] args) {
		// 求一个正整数 倒转后加上1024的结果.
		// 比如 12345倒转后是54321,加上1024，结果是55345.
		Scanner A = new Scanner(System.in);
		System.out.print("输入任意一个整数");
		int a = A.nextInt(), sum = 0;
		for (; a != 0; a /= 10) {
			sum = sum * 10 + a % 10;
		}
		int b = sum + 1024;
		System.out.print(b);
	}

}
