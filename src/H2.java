import java.util.Scanner;

public class H2 {

	public static void main(String[] args) {
		// 将一个正整数分解质因数。例如：输入90,打印出90=2*3*3*5。
		Scanner A = new Scanner(System.in);
		System.out.print("输入一个正整数");
		int a = A.nextInt(), i = 0, b = 0, c = 0;
		while (a % 2 == 0) {
			a = a / 2;
			i++;
		}
		while (a % 3 == 0) {
			a = a / 3;
			b++;
		}
		while (a % 5 == 0) {
			a = a / 5;
			c++;
		}
		System.out.print("a" + "=" + Math.pow(2, i) + "+" + Math.pow(3, b)
				+ "+" + Math.pow(5, c));
	}

}
