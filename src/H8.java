import java.util.Scanner;

public class H8 {

	public static void main(String[] args) {
		// 斐波拉切数列
		// 前两个是1，后面的每一个是前面的两个和
		Scanner n = new Scanner(System.in);
		System.out.print("输入前N项");
		int N = n.nextInt(), c = 0;
		System.out.print("1,1");
		for (int i = 1, a = 1, b = 1; i <= N - 2; i++) {
			c = a + b;
			System.out.print("c");
			a = b;
			b = c;
		}
	}

}
