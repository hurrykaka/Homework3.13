import java.util.Scanner;

public class H7 {

	public static void main(String[] args) {
		// 输出比10000大的10个质数
		int a = 10001;
		for (int i = 2;; i++, a++) {
			if (a % i == 0 && a != i)
				System.out.print(a);
			if (a == 10) {
				break;
			}
		}
	}
}
