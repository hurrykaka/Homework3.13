import java.util.Scanner;

public class H1 {

	public static void main(String[] args) {
		// 输入一个整数，判断其是否是另一个数的平方，
		// 比如64是8的平方，7不是任何数的平方
		Scanner A = new Scanner(System.in);
		System.out.print("输入一个整数");
		int a = A.nextInt();
		int b = (int) Math.sqrt(a);
		if ((Math.pow(b, 2)) == a) {
			System.out.print("a是b的平方");
		} else
			System.out.print("a不是任何数的平方");

	}

}
